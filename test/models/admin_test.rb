require 'test_helper'

class AdminTest < ActiveSupport::TestCase
  test "should not save admins without name and password" do
	 	admin = Admin.new
	 	assert_not admin.save
  end

   test "should not save admins which already exists" do
	 	admin = Admin.new(:username => "admin001", :password => "001admin")
	 	assert_not admin.save
	end

	test "should save admins with name and password" do
		admin = Admin.new(:username => "admin003", :password => "003admin")
		assert admin.save
	end
end
