require 'test_helper'

class SongTest < ActiveSupport::TestCase
    test "should not save song without attributes" do
	 	song = Song.new()
	 	assert_not song.save
	end

	test "should not save song without any of the mandatory attributes-1" do
	 	song = Song.new(:name => "Black", :price => 0.25)
	 	assert_not song.save
	end

	test "should not save song without any of the mandatory attributes-2" do
	 	song = Song.new(:minMin => 10.0, :maxMin => 20.0, :price => 0.10)
	 	assert_not song.save
	end

	test "sould not save song with price less than 0.0" do
		song = Song.new(:name => "Black", :path => "MyPath", :minMin => 10.0, :maxMin => 25.0, :price => -0.10)
		assert_not song.save
	end

	test "should save song with all attributes specified" do
		song = Song.new(:name => "Black", :path => "MyPath", :minMin => 10.0, :maxMin => 25.0, :price => 0.10)
		assert song.save
	end
end
