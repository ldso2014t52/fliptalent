require 'test_helper'

class LocationTest < ActiveSupport::TestCase
	test "should not save location without name" do
		location = Location.new()
		assert_not location.save, "Saved the location without a name"
	end

	test "should not save location with non existing country id" do
		location = Location.new(:name => "Place", :country_id => 50)
		assert_not location.save, "Saved the location with a non existing country id"
	end

	test "should save location with correct attributes" do
		country = Country.new(:name => "Placeburgystan")
		location = Location.new(:name => "Place", :country_id => 31)
		assert location.save, "Did not save the location with correct attributes"
	end
end
