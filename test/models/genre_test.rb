require 'test_helper'

class GenreTest < ActiveSupport::TestCase
  test "should not save genres without name" do
	 	genre = Genre.new
	 	assert_not country.save
  end

   test "should not save genres which already exists" do
	 	genre = Genre.new(:name => "Rock")
	 	assert_not country.save
	end

	test "should save genres that doesn't exist" do
	 	genre = Genre.new(:name => "Pop")
	 	assert genre.save
	end
end
