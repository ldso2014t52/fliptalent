require 'test_helper'

class SongTypeTest < ActiveSupport::TestCase
	test "should not save song_type without attributes" do
	 	song_type = SongType.new()
	 	assert_not song_type.save, "Saved the song_type without attributes"
	end

	test "should not save song_type with non existing ids" do
		song_type = SongType.new(:song_id => 100, :genre_id => 100)
		assert_not song_type.save, "Saved the song_type with non existing ids"
	end

	test "should save unique song_type with song and user ids" do
		user = User.new(:username => "newuser", :password => "porto1", :name => "John Doe", :email => "newemailaddress@gmail.com", :dateBirth => 2014-10-25)
		song = Song.new(:name => "Random Song", :minMin => 20, :maxMin => 50, :price => 1.5, :path => "path", :demo => "demo", :user_id => 11)
		genre = Genre.new(:name => "Bagpipe Surf Pop")
		song_type = SongType.new(:song_id => 31, :genre_id => 6)
		assert song_type.save, "Did not save the song_type correctly"
	end
end
