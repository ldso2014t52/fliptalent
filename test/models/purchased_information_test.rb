require 'test_helper'

class PurchasedInformationTest < ActiveSupport::TestCase
	test "should not save purchased_information without attributes" do
		purchased_information = PurchasedInformation.new()
		assert_not purchased_information.save, "Saved the purchased_information without attributes"
	end

	test "should not save purchased_information with info out of range" do
		purchased_information = PurchasedInformation.new(:buyer => 1, :seller => 7, :info => 6)
		assert_not purchased_information.save, "Saved the purchased_information with info out of range"
	end

	test "should save purchased_information with correct attributes" do
		purchased_information = PurchasedInformation.new(:buyer => 1, :seller => 7, :info => 1)
		assert purchased_information.save, "Did not save the purchased_information with correct attributes"
	end
end
