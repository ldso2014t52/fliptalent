require 'test_helper'

class FavoriteTest < ActiveSupport::TestCase
	test "should not save favorite without attributes" do
	 	favorite = Favorite.new()
	 	assert_not favorite.save, "Saved the favorite without attributes"
	end

	test "should not save favorite with non existing ids" do
		favorite = Favorite.new(:user_id => 100, :song_id => 100)
		assert_not favorite.save, "Saved the favorite with non existing ids"
	end

	test "should save unique favorite with song and user ids" do
		user = User.new(:username => "newuser", :password => "porto1", :name => "John Doe", :email => "newemailaddress@gmail.com", :dateBirth => 2014-10-25)
		song = Song.new(:name => "Random Song", :minMin => 20, :maxMin => 50, :price => 1.5, :path => "path", :demo => "demo", :user_id => 11)
		favorite = Favorite.new(:user_id => 11, :song_id => 31)
		assert favorite.save, "Did not save the favorite correctly"
	end
end
