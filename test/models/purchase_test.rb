require 'test_helper'

class PurchaseTest < ActiveSupport::TestCase
	test "should not save purchase without attributes" do
	 	purchase = Purchase.new()
	 	assert_not purchase.save, "Saved the purchase without attributes"
	end

	test "should not save purchase with non existing ids" do
		purchase = Purchase.new(:user_id => 100, :song_id => 100)
		assert_not purchase.save, "Saved the purchase with non existing ids"
	end

	test "should save unique purchase with song and user ids" do
		user = User.new(:username => "newuser", :password => "porto1", :name => "John Doe", :email => "newemailaddress@gmail.com", :dateBirth => 2014-10-25)
		song = Song.new(:name => "Random Song", :minMin => 20, :maxMin => 50, :price => 1.5, :path => "path", :demo => "demo", :user_id => 11)
		purchase = Favorite.new(:user_id => 11, :song_id => 31)
		assert purchase.save, "Did not save the purchase correctly"
	end
end
