require 'test_helper'

class CountryTest < ActiveSupport::TestCase
 	test "should not save countries without name" do
	 	country = Country.new
	 	assert_not country.save
	end

	test "should not save countries which already exists" do
	 	country = Country.new(:name => "Portugal")
	 	assert_not country.save
	end

	test "should save countries that doesn't exist" do
	 	country = Country.new(:name => "Jamaica")
	 	assert country.save
	end
end
