require 'test_helper'

class UserTest < ActiveSupport::TestCase
 	test "should not save user without attributes" do
	 	user = User.new()
	 	assert_not user.save, "Saved the user without attributes"
	end
	
	test "should not save user with already existing username" do
		user = User.new(:username => "usr", :password => "porto1", :name => "John Doe", :email => "differentaddress@gmail.com", :dateBirth => "2014-10-25")
		assert_not user.save, "Saved the user with an already existing username"
	end

	test "should not save user with already existing email address" do
		user = User.new(:username => "user100", :password => "porto1", :name => "John Doe", :email => "johndoe1@gmail.com", :dateBirth => "2014-10-25")
		assert_not user.save, "Saved the user with an already existing email address"
	end

	test "should not save user with password shorter than 6 characters" do
		user = User.new(:username => "user200", :password => "123", :name => "John Doe", :email => "john200doe@gmail.com", :dateBirth => 2014-10-25)
		assert_not user.save, "Saved the user with a password shorther than 6 characters"
	end

	test "should not save user with username shorter than 3 characters" do
		user = User.new(:username => "us", :password => "porto1", :name => "John Doe", :email => "joh@gmail.com", :dateBirth => 2014-10-25)
		assert_not user.save, "Saved the user with a username shorter than 3 characters"
	end

	test "should save user with all correct attributes" do
		user = User.new(:username => "newuser", :password => "porto1", :name => "John Doe", :email => "newemailaddress@gmail.com", :dateBirth => 2014-10-25)
		assert user.save, "Did not save the user with all attributes correctly filled"
	end
end
