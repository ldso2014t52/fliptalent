require 'test_helper'

class ReportablesControllerTest < ActionController::TestCase
  setup do
    @reportable = reportables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reportables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reportable" do
    assert_difference('Reportable.count') do
      post :create, reportable: { context: @reportable.context, registrationDate: @reportable.registrationDate }
    end

    assert_redirected_to reportable_path(assigns(:reportable))
  end

  test "should show reportable" do
    get :show, id: @reportable
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reportable
    assert_response :success
  end

  test "should update reportable" do
    patch :update, id: @reportable, reportable: { context: @reportable.context, registrationDate: @reportable.registrationDate }
    assert_redirected_to reportable_path(assigns(:reportable))
  end

  test "should destroy reportable" do
    assert_difference('Reportable.count', -1) do
      delete :destroy, id: @reportable
    end

    assert_redirected_to reportables_path
  end
end
