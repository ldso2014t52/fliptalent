require 'test_helper'

class PurchasedInformationsControllerTest < ActionController::TestCase
  setup do
    @purchased_information = purchased_informations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:purchased_informations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create purchased_information" do
    assert_difference('PurchasedInformation.count') do
      post :create, purchased_information: { buyer: @purchased_information.buyer, info: @purchased_information.info, seller: @purchased_information.seller }
    end

    assert_redirected_to purchased_information_path(assigns(:purchased_information))
  end

  test "should show purchased_information" do
    get :show, id: @purchased_information
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @purchased_information
    assert_response :success
  end

  test "should update purchased_information" do
    patch :update, id: @purchased_information, purchased_information: { buyer: @purchased_information.buyer, info: @purchased_information.info, seller: @purchased_information.seller }
    assert_redirected_to purchased_information_path(assigns(:purchased_information))
  end

  test "should destroy purchased_information" do
    assert_difference('PurchasedInformation.count', -1) do
      delete :destroy, id: @purchased_information
    end

    assert_redirected_to purchased_informations_path
  end
end
