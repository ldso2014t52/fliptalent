Rails.application.routes.draw do

root :to => 'sessions#show'
  get 'sessions/new'

  post 'list/followers' => 'followers#create'
  post 'list/users' => 'users#create'
  post 'list/songs' => 'songs#create'
  post 'list/purchases' => 'purchases#create'
  post 'list/votes' => 'votes#create', :as => 'create_vote'
  post 'list/favorites' => 'favorites#create', :as => 'create_favorite'
  post 'list/genres' => 'genres#create'
  post 'list/song_types' => 'song_types#create'
  post 'list/purchased_informations' => 'purchased_informations#create'
  post 'destroy/favorite' => 'favorites#destroy'

  get 'logout' => 'sessions#destroy', :as => 'logout'
  get 'login' => 'sessions#new', :as => 'login'
  get 'search_song' => 'songs#search', :as => 'search_song'
  get 'search_user' => 'users#search', :as => 'search_user'
  get ':username' => 'users#show', :as => 'user_show'
  get 'user/:id' => 'users#show', :as => 'users_show'
  get ':username/edit' => 'users#edit', :as => 'user_edit'
  get 'account/:id' => 'users#edit', :as => 'account'
  get ':username/favorites' => 'favorites#userfav', :as => 'user_favorites'

  get 'list/users' => 'users#index', :as => 'users'
  get 'list/followers' => 'followers#index', :as => 'followers'
  get 'list/songs' => 'songs#index', :as => 'songs'
  get 'list/followers_new' => 'followers#new', :as => 'followers_new'
  get 'list/votes' => 'votes#index', :as => 'votes'
  get 'list/purchases' => 'purchases#index', :as => 'purchases'
  get 'list/favoritos' => 'favorites#index', :as => 'favorites'
  get 'list/genres' => 'genres#index', :as => 'genres'
  get 'list/song_types' => 'song_types#index', :as => 'song_types'
  get 'list/purchased_informations' => 'purchased_informations#index', :as => 'purchased_informations'

  post 'new_favorite' => 'favorites#resolve_layout'
  
  resources :report_types

  resources :purchased_informations

  resources :followers

  resources :song_types

  resources :purchases

  resources :votes

  resources :songs

  resources :users

  resources :favorites
  #, param: :username do
   #resources :favorites
  #end

  resources :reports

  resources :locations

  resources :reportables

  resources :genres

  resources :countries

  resources :admins

  resources :sessions




  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
