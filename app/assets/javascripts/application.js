// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require audio
//= require dropzone
//= require jquery_ujs
//= require jquery-ui
//= require foundation
//= require modernizr
//= require turbolinks

function show_song_name(){
	$('#user_songs li').hover(function() {
		$('.song_name').text($(this).attr('data-name'));
	},function() {
		$('.song_name').text('');
	});
}

$(function(){ $(document).foundation(); });
show_song_name();

function tabsAnimation(id_tab){
	$("div.tabs-content div.content").css('opacity',0);
	$('div#'+id_tab).css('opacity',1);		

	if(id_tab === 'home')
		$("dl.tabs").css('background-position-x',0);
	else
		if(id_tab === 'genre')
			$("dl.tabs").css('background-position-x',100);
		else
			if(id_tab === 'popular')
				$("dl.tabs").css('background-position-x',200);
		}
		function myFunction(){
			var x = document.getElementById("song_path");
			if ('files' in song_path) {
				if (x.files.length == 0) {
					txt = "Select one or more files.";
				} else {
					for (var i = 0; i < x.files.length; i++) {
						var file = x.files[i];
						if ('name' in file) {
							$('div.dz-filename span').text(file.name)
						}
						if ('size' in file) {
							$('div.dz-size strong').text((file.size/1000000).toFixed(2));
						}
					}
				}
				$('.dz-success-mark').css("visibility","visible");
			} 
			else {
				if (x.value == "") {
					txt += "Select one or more files.";
				} else {
					txt += "The files property is not supported by your browser!";
            txt  += "<br>The path of the selected file: " + x.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
        }
    }
}

$(".ed_tab").tabs({ 
	show: { effect: "slide", direction: "left", duration: 200, easing: "easeOutBack" } ,
	hide: { effect: "slide", direction: "right", duration: 200, easing: "easeInQuad" } 
});