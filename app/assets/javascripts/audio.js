function play_pause_Handle(track_id){

	if($('div#track'+track_id+' .player-btn').hasClass('paused')){

		$('.audio_track').get(0).pause();

		$('.player-btn').removeClass('playing');
		$('.player-btn').addClass('paused');

		$('div#track'+track_id+' .player-btn').removeClass('paused');
		$('div#track'+track_id+' .player-btn').addClass('playing');

		changeTrack(track_id,$('div#track'+track_id).attr('data-src'));

		$('.audio_track').get(0).play();

	}
	else if ($('div#track'+track_id+' .player-btn').hasClass('playing')){
		$('div#track'+track_id+' .player-btn').removeClass('playing');
		$('div#track'+track_id+' .player-btn').addClass('paused');

		$('.audio_track').get(0).pause();
	}
	else{
		$('div#track'+track_id+' .player-btn').addClass('paused');
		$('.audio_track').get(0).pause();
	}
}

function changeTrack(track_id, trackPath){
	$('.audio_track').attr('src',trackPath);
	$('.audio_track').attr('data-track',track_id);

	if($('div#track'+track_id).attr('data-interval') != ""){
		var interval = JSON.parse($('div#track'+track_id).attr('data-interval'));
		$('.audio_track').get(0).currentTime = interval[0];

		function verifyEndTime(){
			if($('.audio_track').get(0).currentTime >= interval[1]){
				$('.audio_track').get(0).currentTime = interval[0];
				play_pause_Handle(track_id);
			}

			progress_bar_handler_limited_track(track_id, interval[0], interval[1]);
		}
		$('.audio_track').on('timeupdate', verifyEndTime);
	}
	else{
		function updateProgressBar(){
			progress_bar_handler(track_id);
		}

		$('.audio_track').on('timeupdate',updateProgressBar);
	}

}

function getSecondsToString_Parser(sec){
	var timeString = {min:'00',sec:'00'};

	if(Math.floor(sec/60) < 10)
		timeString.min = '0'+Math.floor(sec/60);
	else
		timeString.min = Math.floor(sec/60);

	if(Math.floor(sec)%60 < 10)
		timeString.sec = '0'+Math.floor(sec)%60;
	else
		timeString.sec = Math.floor(sec)%60;

	return timeString;
}

function progress_bar_handler(track_id){

	if($('.audio_track').attr('data-track')==track_id){
		var currentTime = $('.audio_track').get(0).currentTime;
		var duration = $('.audio_track').get(0).duration;

		var per = (currentTime/duration)*100+'%';
		var scrubber = ((currentTime/duration)*100-0.5)+'%';
		var buffered = ($('.audio_track').get(0).buffered.end(0)/duration)*100+'%';

		$('div#track'+track_id+' #elapsed').css('width', per);
		$('div#track'+track_id+' #scrubber').css('left', scrubber);
		$('div#track'+track_id+' #buffered').css('width', buffered);

		var timeElapsed = getSecondsToString_Parser(currentTime);

		$('div#track'+track_id+' #time-elapsed').text(timeElapsed.min+':'+timeElapsed.sec);

		var timeTotal = getSecondsToString_Parser(duration);

		$('div#track'+track_id+' #time-total').text(timeTotal.min+':'+timeTotal.sec);
	}
}

function progress_bar_handler_limited_track(track_id, startTime, endTime){

	if($('.audio_track').attr('data-track')==track_id){
		var currentTime = $('.audio_track').get(0).currentTime;
		var duration = $('.audio_track').get(0).duration;

		if(duration > endTime)
			duration = endTime-startTime;

		var per = ((currentTime-startTime)/duration)*100+'%';
		var scrubber = (((currentTime-startTime)/duration)*100-0.5)+'%';
		if($('.audio_track').get(0).buffered.end(0) > endTime)
			var buffered = '100%';
		else
			var buffered = (($('.audio_track').get(0).buffered.end(0)-startTime)/duration)*100+'%';
		
		$('div#track'+track_id+' #elapsed').css('width', per);
		$('div#track'+track_id+' #scrubber').css('left', scrubber);
		$('div#track'+track_id+' #buffered').css('width', buffered);

		var timeElapsed = getSecondsToString_Parser(currentTime-startTime);

		$('div#track'+track_id+' #time-elapsed').text(timeElapsed.min+':'+timeElapsed.sec);

		var timeTotal = getSecondsToString_Parser(duration);

		$('div#track'+track_id+' #time-total').text(timeTotal.min+':'+timeTotal.sec);
	}
}

function vote_like(track_id){

	if($('div#track'+track_id+' .feedback').hasClass('like'))
		$('div#track'+track_id+' .feedback').removeClass('like');
	else 
		if($('div#track'+track_id+' .feedback').hasClass('dislike')){
			$('div#track'+track_id+' .feedback').addClass('like');
			$('div#track'+track_id+' .feedback').removeClass('dislike');
		}
		else
			$('div#track'+track_id+' .feedback').addClass('like');
	}
	function vote_dislike(track_id){

		if($('div#track'+track_id+' .feedback').hasClass('dislike'))
			$('div#track'+track_id+' .feedback').removeClass('dislike');
		else 
			if($('div#track'+track_id+' .feedback').hasClass('like')){
				$('div#track'+track_id+' .feedback').addClass('dislike');
				$('div#track'+track_id+' .feedback').removeClass('like');
			}
			else
				$('div#track'+track_id+' .feedback').addClass('dislike');

		}

		var CURRENT_TRACK_ID = 0;

		function addTrack(filePath, trackName, autorName, options){

			var _temp_track =       '<div id="track'+CURRENT_TRACK_ID+'" class="track" data-src="'+filePath+'" data-interval="">\
			<div id="player" class="playing">\
			<div id="now-playing">\
			<div id="play-controls">\
			<a id="play-pause" class="player-btn player-icon paused" onclick="play_pause_Handle('+CURRENT_TRACK_ID+')">\
			<span data-translate-text="PLAY" style="display: none;">Play</span>\
			</a>\
			</div>\
			<div class="img-container" style="display:none;">\
			<img width="40" height="40" id="now-playing-image" class="img" src="http://images.gs-cdn.net/static/albums/40_album.png">\
			</div>\
			<div class="inner">\
			<div>\
			<div class="metadata">\
			<a href="#"class="now-playing-link song song-link" data-reveal-id="'+trackName+'_'+CURRENT_TRACK_ID+'_modal"></a>\
			<div class="data-container">\
			<span>&nbsp;by&nbsp;</span>\
			<a class="now-playing-link artist"></a>\
			</div>\
			</div>\
			</div>\
			<span id="time-elapsed" class="now-playing-time">00:00</span>\
			<div id="progress-bar">\
			<div id="scrubber" style="left: -2.89905805112578px;"></div>\
			<div id="buffered" style="width: 0%;-webkit-box-sizing:initial"></div>\
			<div id="elapsed"  style="width: 1.311296130816%;-webkit-box-sizing:initial"></div>\
			</div>\
			<span id="time-total" class="now-playing-time">00:00</span>\
			</div>\
			<div class="feedback">\
			<a onclick="vote_like('+CURRENT_TRACK_ID+')"><span class="fi-like"></span></a>\
			<a onclick="vote_dislike('+CURRENT_TRACK_ID+')"><span class="fi-dislike"></span></a>\
			</div>\
			</div>\
			</div>\
			</div>';

			$('.track_playlist').append(_temp_track);

			var track_id = CURRENT_TRACK_ID;

			if(options != null){

				$('div#track'+track_id).attr('data-interval','['+options.startTime+','+options.endTime+']');
				$('div#track'+track_id+' a.song').append(''+trackName);
				$('div#track'+track_id+' a.artist').text('Unknown');
			}
			else{

				$('div#track'+track_id+' div#elapsed').addClass('purchased');
				$('div#track'+track_id+' a.song').append(''+trackName);
				$('div#track'+track_id+' a.artist').append(''+autorName);
				$('div#track'+track_id+' .inner .data-container').append('<span class="fi-check" style="text-shadow: black 1px 0px 1px;color: #cc9933;">&nbspPurchased</span>');
			}

			CURRENT_TRACK_ID++;
		}