json.array!(@songs) do |song|
  json.extract! song, :id, :reportable_id, :name, :minMin, :maxMin, :price, :path, :demo, :user_id
  json.url song_url(song, format: :json)
end
