json.array!(@users) do |user|
  json.extract! user, :id, :reportable_id, :username, :password, :name, :address, :email, :money, :contact, :dateBirth, :observations, :bronze, :silver, :gold, :location_id
  json.url user_url(user, format: :json)
end
