json.array!(@reports) do |report|
  json.extract! report, :id, :reason, :checked, :reportable_id, :admin_id
  json.url report_url(report, format: :json)
end
