json.array!(@song_types) do |song_type|
  json.extract! song_type, :id, :song_id, :genre_id
  json.url song_type_url(song_type, format: :json)
end
