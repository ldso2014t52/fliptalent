json.array!(@followers) do |follower|
  json.extract! follower, :id, :follower, :followed
  json.url follower_url(follower, format: :json)
end
