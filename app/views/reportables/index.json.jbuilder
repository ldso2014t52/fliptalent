json.array!(@reportables) do |reportable|
  json.extract! reportable, :id, :registrationDate, :context
  json.url reportable_url(reportable, format: :json)
end
