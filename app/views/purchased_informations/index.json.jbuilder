json.array!(@purchased_informations) do |purchased_information|
  json.extract! purchased_information, :id, :buyer, :seller, :info
  json.url purchased_information_url(purchased_information, format: :json)
end
