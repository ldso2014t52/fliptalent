class Song < ActiveRecord::Base
  mount_uploader :path, SongUploader
  mount_uploader :demo, AvatarUploader
  belongs_to :reportable
  belongs_to :user

  has_many :genres, through: :song_types
  has_many :users, through: :purchases
  has_many :users, through: :votes
  has_many :users, through: :favorites
  
  validates :name, presence: true

  validates :path, presence: true
  validates :price, presence: true, numericality: {greater_than_or_equal: 0.0}
  validates :minMin, presence: true
  validates :maxMin, presence: true
  #validates :genre, presence: true 
  validate :minimum_time_error
  #validate :maximum_time_error


  def minimum_time_error
    if maxMin - minMin > 30
      errors.add(:minMin, :maxMin, "Error")
    end
  end

  #def maximum_time_error
    #if maxMin - 30 > minMin
  #     errors.add("Error")
   #   end
 # end﻿
  
end
