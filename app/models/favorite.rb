class Favorite < ActiveRecord::Base
  # self.primary_keys = :user_id, :song_id 
  belongs_to :user
  belongs_to :song
end
