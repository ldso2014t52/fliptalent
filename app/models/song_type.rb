class SongType < ActiveRecord::Base
  # self.primary_keys = :song_id, :genre_id
  belongs_to :song
  belongs_to :genre
end
