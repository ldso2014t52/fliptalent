class Follower < ActiveRecord::Base
	belongs_to :user, foreign_key: "follower_id"
	belongs_to :user, foreign_key: "followed_id"
end
