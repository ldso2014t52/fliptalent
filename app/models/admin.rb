class Admin < ActiveRecord::Base
	has_many :reports
	
	validates :username, presence: true, uniqueness: true
	validates :password, presence: true

        def self.authenticate(email, password)
    admin = Admin.find_by username: email
    if admin && admin.password == password
      admin
    else
      nil
    end
  end

end
