class Purchase < ActiveRecord::Base
  #self.primary_keys = :user_id, :song_id
  belongs_to :user
  belongs_to :song

 def paypal_url(return_path)
    values = {
        business: user.email,
        cmd: "_xclick",
        upload: 1,
        return: "#{Rails.application.secrets.app_host}#{return_path}",
        invoice: id,
        amount: song.price,
        item_name: song.name,
        item_number: song.id,
        quantity: '1'
    }
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end

end
