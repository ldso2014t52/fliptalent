class ReportType < ActiveRecord::Base
	has_many :reports

	validates :tag, presence: true
end
