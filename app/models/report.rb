class Report < ActiveRecord::Base
  belongs_to :reportable
  belongs_to :admin
  belongs_to :report_type

  validates :reason, presence: true
end
