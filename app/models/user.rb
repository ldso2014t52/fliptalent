class User < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader
  belongs_to :reportable
  belongs_to :location

  
  after_initialize :default_values
  before_save :encrypt_password

  
  has_many :users, through: :followers
  has_many :users, through: :purchased_informations
  has_many :songs, through: :purchases
  has_many :songs, through: :votes
  has_many :songs, through: :favorites

  validates :username, uniqueness: true, presence: true, length: {minimum: 3}
  validates :password, presence: true, length: {minimum: 6}
  validates :name, presence: true
  validates :email, uniqueness: true, presence: true
  validates :dateBirth, presence: true
  validates :money, presence: true
  validates :gold, presence: true 
  validates :silver, presence: true
  validates :bronze, presence: true
  validates_confirmation_of :password

    def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
   def default_values
    self.money ||= 0
    self.gold ||= 0
    self.silver ||= 0
    self.bronze ||= 0
    self.created_at ||= Time.now
  end
end
