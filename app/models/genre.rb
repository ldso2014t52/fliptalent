class Genre < ActiveRecord::Base
	has_many :songs, through: :song_types
	
	validates :name, uniqueness: true, presence: true
end
