class PurchasedInformation < ActiveRecord::Base
	# self.primary_keys = :user1_id, :user2_id
	
	belongs_to :user, foreign_key: "buyer_id"
	belongs_to :user, foreign_key: "seller_id"
	
	validates :info, presence: true, :inclusion => {:in => [0,1,2]}
end
