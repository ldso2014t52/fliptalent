class Reportable < ActiveRecord::Base
	has_many :reports

	validates :context, presence: true, :inclusion => {:in => [0,1]}
end
