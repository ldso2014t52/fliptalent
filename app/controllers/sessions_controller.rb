class SessionsController < ApplicationController
  def new
    @countries = Country.all
    @user = User.new
    @admin = Admin.new
  end

  def show
  	if session[:user_id] == nil
  		redirect_to login_url
    else
       @user = User.find(session[:user_id])
  	end
  end

  def create
  user = User.authenticate(params[:email], params[:password])
  admin = Admin.authenticate(params[:email], params[:password])
  if user
    session[:user_id] = user.id
    redirect_to root_url, :notice => "Logged in!"
  else if admin
    session[:admin_id] = admin.id
    redirect_to users_url, :notice => "Logged in!"
  else 
    flash.now.alert = "Invalid email or password"
    redirect_to root_url, :notice => "falha"
  end
end
end




def destroy
  session[:user_id] = nil
  redirect_to root_url, :notice => "Logged out!"
end
end
