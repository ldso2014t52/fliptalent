class ReportablesController < ApplicationController
  before_action :set_reportable, only: [:show, :edit, :update, :destroy]

  # GET /reportables
  # GET /reportables.json
  def index
    @reportables = Reportable.all
  end

  # GET /reportables/1
  # GET /reportables/1.json
  def show
  end

  # GET /reportables/new
  def new
    @reportable = Reportable.new
  end

  # GET /reportables/1/edit
  def edit
  end

  # POST /reportables
  # POST /reportables.json
  def create
    @reportable = Reportable.new(reportable_params)

    respond_to do |format|
      if @reportable.save
        format.html { redirect_to @reportable, notice: 'Reportable was successfully created.' }
        format.json { render :show, status: :created, location: @reportable }
      else
        format.html { render :new }
        format.json { render json: @reportable.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reportables/1
  # PATCH/PUT /reportables/1.json
  def update
    respond_to do |format|
      if @reportable.update(reportable_params)
        format.html { redirect_to @reportable, notice: 'Reportable was successfully updated.' }
        format.json { render :show, status: :ok, location: @reportable }
      else
        format.html { render :edit }
        format.json { render json: @reportable.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reportables/1
  # DELETE /reportables/1.json
  def destroy
    @reportable.destroy
    respond_to do |format|
      format.html { redirect_to reportables_url, notice: 'Reportable was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reportable
      @reportable = Reportable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reportable_params
      params.require(:reportable).permit(:registrationDate, :context)
    end
end
