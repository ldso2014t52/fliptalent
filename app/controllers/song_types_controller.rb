class SongTypesController < ApplicationController
  before_action :set_song_type, only: [:show, :edit, :update, :destroy]

  # GET /song_types
  # GET /song_types.json
  def index
    @song_types = SongType.all
  end

  # GET /song_types/1
  # GET /song_types/1.json
  def show
  end

  # GET /song_types/new
  def new
    @song_type = SongType.new
  end

  # GET /song_types/1/edit
  def edit
  end

  # POST /song_types
  # POST /song_types.json
  def create
    @song_type = SongType.new(song_type_params)

    respond_to do |format|
      if @song_type.save
        format.html { redirect_to :back, notice: 'The genre '+@song_type.genre.name+' was successfully added to '+@song_type.song.name+'.' }
        format.json { render :show, status: :created, location: @song_type }
      else
        format.html { render :new }
        format.json { render json: @song_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /song_types/1
  # PATCH/PUT /song_types/1.json
  def update
    respond_to do |format|
      if @song_type.update(song_type_params)
        format.html { redirect_to @song_type, notice: 'Song type was successfully updated.' }
        format.json { render :show, status: :ok, location: @song_type }
      else
        format.html { render :edit }
        format.json { render json: @song_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /song_types/1
  # DELETE /song_types/1.json
  def destroy
    @song_type.destroy
    respond_to do |format|
      format.html { redirect_to song_types_url, notice: 'Song type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_song_type
      @song_type = SongType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def song_type_params
      params.require(:song_type).permit(:song_id, :genre_id)
    end
end
