class UsersController < ApplicationController
  before_action :set_user, only: [:update, :destroy]


  def upload
  uploaded_io = params[:person][:picture]
    File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
      file.write(uploaded_io.read)
    end
  end

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
      if params[:username]
        @user = User.where(username: params[:username]).first
      else
        set_user
        redirect_to(user_show_path(@user.username))
      end

      if (PurchasedInformation.where(buyer:session[:user_id],seller:@user.id).first or session[:user_id] == @user.id)
        @username = @user.name
        @user_image = @user.avatar_url
      else
        @username = 'Unknown'
        @user_image = 'default_user.png'
      end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
      if params[:username]
        @user = User.where(username: params[:username]).first
      else
        @user = User.find(params[:id])
      end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to root_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search
      if user_params[:nationality] != ""
      @search = User.where("nationality LIKE :snationality", {:snationality => "#{user_params[:nationality]}"})
    elsif user_params[:influencia] != ""
      @search = User.where("influencia LIKE :sinfluencia", {:sinfluencia => "#{user_params[:influencia]}"})
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:reportable_id, :username, :password, :name, :address, :email, :avatar, :money, :contact, :dateBirth, :bio, :bronze, :silver, :gold, :location_id, :nationality, :facebookurl, :twitterurl, :influencia)
    end
end
