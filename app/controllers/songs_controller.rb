class SongsController < ApplicationController
  before_action :set_song, only: [:show, :edit, :update, :destroy]

  # GET /songs
  # GET /songs.json
  def index
    @songs = Song.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users}
    end
  end

  # GET /songs/1
  # GET /songs/1.json
  def show
  end

  # GET /songs/new
  def new
    @song = Song.new
  end

  # GET /songs/1/edit
  def edit
  end

  # POST /songs
  # POST /songs.json
  def create
    x = song_params
    user = User.find(session[:user_id])
    x[:user] = user
    @song = Song.create(x)

    respond_to do |format|
      if @song.save
        format.html { redirect_to user_show_path(user[:username]), notice: 'Song was successfully created.' } # temporario => alterar redirecionamento
        format.json { render :show, status: :created, location: @song }
      else
        format.html { render :new }
        format.json { render json: @song.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /songs/1
  # PATCH/PUT /songs/1.json
  def update
    respond_to do |format|
      if @song.update(song_params)
        format.html { redirect_to @song, notice: 'Song was successfully updated.' }
        format.json { render :show, status: :ok, location: @song }
      else
        format.html { render :edit }
        format.json { render json: @song.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /songs/1
  # DELETE /songs/1.json
  def destroy
    @status_update = Song.find(params[:id])
    if @status_update.present?
      @status_update.destroy
    end
    redirect_to songs_path
  end

  def search
    if song_params[:name] != "" && song_params[:price] != "" && song_params[:main_influence] != "" && song_params[:genre] != "Song genre"
      @search = Song.where("name LIKE :sname AND price = :sprice AND main_influence = :smain_influence AND genre = :sgenre", {:sname => "%#{song_params[:name]}%", :sprice => "#{song_params[:price]}", :smain_influence => "#{song_params[:main_influence]}", :sgenre => "#{song_params[:genre]}"})
    elsif song_params[:name] != "" && song_params[:price] == "" && song_params[:main_influence] == "" && song_params[:genre] == "Song genre"
      @search = Song.where("name LIKE :sname", {:sname => "%#{song_params[:name]}%"})
    elsif song_params[:name] == "" && song_params[:price] != "" && song_params[:main_influence] == "" && song_params[:genre] == "Song genre"
      @search = Song.where("price = :sprice", {:sprice => "#{song_params[:price]}"})
    elsif song_params[:name] == "" && song_params[:price] == "" && song_params[:main_influence] == "" && song_params[:genre] != "Song genre"
      @search = Song.where("genre = :sgenre", {:sgenre => "#{song_params[:genre]}"})
    elsif song_params[:name] == "" && song_params[:price] == "" && song_params[:main_influence] != "" && song_params[:genre] == "Song genre"
      @search = Song.where("main_influence = :smain_influence", {:smain_influence => "#{song_params[:main_influence]}"})
    elsif song_params[:name] != "" && song_params[:price] != "" && song_params[:main_influence] == "" && song_params[:genre] == "Song genre"
      @search = Song.where("name LIKE :sname AND price = :sprice", {:sname => "%#{song_params[:name]}%", :sprice => "#{song_params[:price]}"})
    elsif song_params[:name] != "" && song_params[:price] == "" && song_params[:main_influence] == "" && song_params[:genre] != "Song genre"
      @search = Song.where("name LIKE :sname AND genre = :sgenre", {:sname => "%#{song_params[:name]}%", :sgenre => "#{song_params[:genre]}"})
    elsif song_params[:name] != "" && song_params[:price] == "" && song_params[:main_influence] != "" && song_params[:genre] == "Song genre"
      @search = Song.where("name LIKE :sname AND main_influence = :smain_influence", {:sname => "%#{song_params[:name]}%", :smain_influence => "#{song_params[:main_influence]}"})
    elsif song_params[:name] == "" && song_params[:price] != "" && song_params[:main_influence] == "" && song_params[:genre] != "Song genre"
      @search = Song.where("price = :sprice AND genre = :sgenre", {:sprice => "#{song_params[:price]}", :sgenre => "#{song_params[:genre]}"})
    elsif song_params[:name] == "" && song_params[:price] != "" && song_params[:main_influence] != "" && song_params[:genre] == "Song genre"
      @search = Song.where("price = :sprice AND main_influence = :smain_influence", {:sprice => "#{song_params[:price]}", :smain_influence => "#{song_params[:main_influence]}"})
    elsif song_params[:name] == "" && song_params[:price] == "" && song_params[:main_influence] != "" && song_params[:genre] != "Song genre"
      @search = Song.where("genre = :sgenre AND main_influence = :smain_influence", {:sgenre => "#{song_params[:genre]}", :smain_influence => "#{song_params[:main_influence]}"})
    elsif song_params[:name] != "" && song_params[:price] != "" && song_params[:main_influence] != "" && song_params[:genre] == "Song genre"
      @search = Song.where("name LIKE :sname AND price = :sprice AND main_influence = :smain_influence", {:sname => "%#{song_params[:name]}%", :sprice => "#{song_params[:price]}", :smain_influence => "#{song_params[:main_influence]}"})
    elsif song_params[:name] != "" && song_params[:price] != "" && song_params[:main_influence] == "" && song_params[:genre] != "Song genre"
      @search = Song.where("name LIKE :sname AND price = :sprice AND genre = :sgenre", {:sname => "%#{song_params[:name]}%", :sprice => "#{song_params[:price]}", :sgenre => "#{song_params[:genre]}"})
    elsif song_params[:name] != "" && song_params[:price] == "" && song_params[:main_influence] != "" && song_params[:genre] != "Song genre"
      @search = Song.where("name LIKE :sname AND genre = :sgenre AND main_influence = :smain_influence", {:sname => "%#{song_params[:name]}%", :sgenre => "#{song_params[:genre]}", :smain_influence => "#{song_params[:main_influence]}"})
    elsif song_params[:name] == "" && song_params[:price] != "" && song_params[:main_influence] != "" && song_params[:genre] != "Song genre"
      @search = Song.where("genre = :sgenre AND price = :sprice AND main_influence = :smain_influence", {:sgenre => "#{song_params[:genre]}", :sprice => "#{song_params[:price]}", :smain_influence => "#{song_params[:main_influence]}"})
    end
  end

  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_song
      @song = Song.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def song_params
      params.require(:song).permit(:user, :name, :minMin, :maxMin, :price, :path, :main_influence, :genre, :demo)
    end
end
