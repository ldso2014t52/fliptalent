class PurchasedInformationsController < ApplicationController
  before_action :set_purchased_information, only: [:show, :edit, :update, :destroy]

  # GET /purchased_informations
  # GET /purchased_informations.json
  def index
    @purchased_informations = PurchasedInformation.all
  end

  # GET /purchased_informations/1
  # GET /purchased_informations/1.json
  def show
  end

  # GET /purchased_informations/new
  def new
    @purchased_information = PurchasedInformation.new
  end

  # GET /purchased_informations/1/edit
  def edit
  end

  # POST /purchased_informations
  # POST /purchased_informations.json
  def create
    @purchased_information = PurchasedInformation.new(purchased_information_params)

    respond_to do |format|
      if @purchased_information.save
        format.html { redirect_to @purchased_information, notice: 'Purchased information was successfully created.' }
        format.json { render :show, status: :created, location: @purchased_information }
      else
        format.html { render :new }
        format.json { render json: @purchased_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /purchased_informations/1
  # PATCH/PUT /purchased_informations/1.json
  def update
    respond_to do |format|
      if @purchased_information.update(purchased_information_params)
        format.html { redirect_to @purchased_information, notice: 'Purchased information was successfully updated.' }
        format.json { render :show, status: :ok, location: @purchased_information }
      else
        format.html { render :edit }
        format.json { render json: @purchased_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchased_informations/1
  # DELETE /purchased_informations/1.json
  def destroy
    @purchased_information.destroy
    respond_to do |format|
      format.html { redirect_to purchased_informations_url, notice: 'Purchased information was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchased_information
      @purchased_information = PurchasedInformation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchased_information_params
      params.require(:purchased_information).permit(:buyer, :seller, :info)
    end
end
