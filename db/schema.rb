# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141127095641) do

  create_table "admins", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "favorites", force: true do |t|
    t.integer  "user_id"
    t.integer  "song_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "favorites", ["song_id"], name: "index_favorites_on_song_id"
  add_index "favorites", ["user_id"], name: "index_favorites_on_user_id"

  create_table "followers", force: true do |t|
    t.integer  "follower"
    t.integer  "followed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "genres", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations", ["country_id"], name: "index_locations_on_country_id"

  create_table "purchased_informations", force: true do |t|
    t.integer  "buyer"
    t.integer  "seller"
    t.integer  "info"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchases", force: true do |t|
    t.integer  "user_id"
    t.integer  "song_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "purchases", ["song_id"], name: "index_purchases_on_song_id"
  add_index "purchases", ["user_id"], name: "index_purchases_on_user_id"

  create_table "report_types", force: true do |t|
    t.string   "tag"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reportables", force: true do |t|
    t.datetime "registrationDate"
    t.integer  "context"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reports", force: true do |t|
    t.string   "reason"
    t.boolean  "checked"
    t.integer  "reportable_id"
    t.integer  "admin_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reports", ["admin_id"], name: "index_reports_on_admin_id"
  add_index "reports", ["reportable_id"], name: "index_reports_on_reportable_id"

  create_table "song_types", force: true do |t|
    t.integer  "song_id"
    t.integer  "genre_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "song_types", ["genre_id"], name: "index_song_types_on_genre_id"
  add_index "song_types", ["song_id"], name: "index_song_types_on_song_id"

  create_table "songs", force: true do |t|
    t.integer  "reportable_id"
    t.string   "name"
    t.float    "minMin"
    t.float    "maxMin"
    t.float    "price"
    t.string   "path"
    t.string   "demo"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "main_influence"
    t.string   "genre"
    t.string   "description"
  end

  add_index "songs", ["reportable_id"], name: "index_songs_on_reportable_id"
  add_index "songs", ["user_id"], name: "index_songs_on_user_id"

  create_table "users", force: true do |t|
    t.integer  "reportable_id"
    t.string   "username"
    t.string   "password"
    t.string   "name"
    t.string   "address"
    t.string   "email"
    t.float    "money"
    t.string   "contact"
    t.date     "dateBirth"
    t.string   "bio"
    t.float    "bronze"
    t.float    "silver"
    t.float    "gold"
    t.integer  "location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "facebookurl"
    t.string   "twitterurl"
    t.string   "nationality"
    t.string   "influencia"
  end

  add_index "users", ["location_id"], name: "index_users_on_location_id"
  add_index "users", ["reportable_id"], name: "index_users_on_reportable_id"

  create_table "votes", force: true do |t|
    t.integer  "user_id"
    t.integer  "song_id"
    t.boolean  "vote"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["song_id"], name: "index_votes_on_song_id"
  add_index "votes", ["user_id"], name: "index_votes_on_user_id"

end
