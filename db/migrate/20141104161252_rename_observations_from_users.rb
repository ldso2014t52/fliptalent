class RenameObservationsFromUsers < ActiveRecord::Migration
  def change
  	rename_column :users, :observations, :bio
  end
end
