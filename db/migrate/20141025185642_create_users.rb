class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.references :reportable, index: true
      t.string :username
      t.string :password
      t.string :email
      t.string :name
      t.string :address
      t.float :money
      t.string :contact
      t.date :dateBirth
      t.string :observations
      t.float :bronze
      t.float :silver
      t.float :gold
      t.references :location, index: true

      t.timestamps
    end
  end
end
