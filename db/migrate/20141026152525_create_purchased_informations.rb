class CreatePurchasedInformations < ActiveRecord::Migration
  def change
    create_table :purchased_informations do |t|
      t.integer :buyer
      t.integer :seller
      t.integer :info

      t.timestamps
    end
  end
end
