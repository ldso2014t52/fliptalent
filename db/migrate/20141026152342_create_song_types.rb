class CreateSongTypes < ActiveRecord::Migration
  def change
    create_table :song_types do |t|
      t.references :song, index: true
      t.references :genre, index: true

      t.timestamps
    end
  end
end
