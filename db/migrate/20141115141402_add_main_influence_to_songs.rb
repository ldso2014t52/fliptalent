class AddMainInfluenceToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :main_influence, :string
  end
end
