class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.references :reportable, index: true
      t.string :name
      t.float :minMin
      t.float :maxMin
      t.float :price
      t.string :path
      t.string :demo
      t.references :user, index: true

      t.timestamps
    end
  end
end
