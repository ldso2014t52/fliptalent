class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :reason
      t.boolean :checked
      t.references :reportable, index: true
      t.references :admin, index: true

      t.timestamps
    end
  end
end
