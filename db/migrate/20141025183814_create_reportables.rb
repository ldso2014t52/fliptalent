class CreateReportables < ActiveRecord::Migration
  def change
    create_table :reportables do |t|
      t.datetime :registrationDate
      t.integer :context

      t.timestamps
    end
  end
end
